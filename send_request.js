const doctorDivItem= document.querySelector('#container')

const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');
console.log(id); 
const userId=localStorage.getItem('id')
console.log(userId)
async function doctorShow()
{
    let response;
    try{
      response= await fetch(`http://127.0.0.1:8000/api/chats/${id}/single_doctor`,{
        method: 'GET',
        
      });
    }
    catch(error){
        alert('something go wrong!');
        return;
    }
   if(!response.ok){
    alert('something went wrong');
    return;
}
const responseData = await response.json();
const doctor= responseData.doctor;
console.log(doctor)
 createDoctorCard(doctor[0].id,doctor[0].name,doctor[0].media,doctor[0].educational_certificate)

function createDoctorCard(doctorId,doctorName,doctorImage,education){
    let id= doctorDivItem.dataset.doctorId=doctorId;
    const newDoctorImage=document.createElement('img');
    newDoctorImage.src  =doctorImage[0].original_url;
    const newDoctornameElement=document.createElement('h2')
    newDoctornameElement.textContent=doctorName;
    const newDoctorEducationElement=document.createElement('p');
    newDoctorEducationElement.textContent=education;
    doctorDivItem.appendChild(newDoctorImage);
    doctorDivItem.appendChild(newDoctornameElement);
    doctorDivItem.appendChild(newDoctorEducationElement);
}

}

async function sendRequest()
{
  const req= document.getElementById('issue').value;
    let response;
const requsetData={
  req:req,
  user_id:userId,
  doctor_id:id,
};


console.log(JSON.stringify(requsetData) );
try {
  response = await fetch('http://127.0.0.1:8000/api/chats/send_request', {
    method: 'POST',
    body: JSON.stringify(requsetData),
    headers: {
      'Content-Type': 'application/json',
    },
  });
} catch (error) {
  alert('Something went wrong!');
  return;
}

if (!response.ok) {
  alert('Something went wrong!');
  return;
}
const responseData = await response.json();

console.log(responseData);
const successMessage = document.createElement('p');
successMessage.textContent = 'Signup successful! Redirecting to the homepage...';
document.getElementById('succeed').appendChild(successMessage);
setTimeout(() => {
  window.location.href = '/hompage/index.html';
    }, 2000); 
}

document.getElementById('submit').addEventListener('click',sendRequest)

doctorShow();


