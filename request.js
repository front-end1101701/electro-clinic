let doctorOrderList=document.querySelector('#doctor_container');

const sr = ScrollReveal({
    origin: 'bottom',
    distance: '60px',
    duration: 2500,
    delay: 500,
    reset:true,
  })

async function doctorInformation()
{
    let response;
    try{
      response= await fetch('http://127.0.0.1:8000/api/chats/doctor_information',{
        method: 'GET',
        
      });
    }
    catch(error){
        alert('something go wrong!');
        return;
    }
   if(!response.ok){
    alert('something went wrong');
    return;
}
const responseData = await response.json();
const doctors= responseData.doctor
console.log(doctors);
// console.log(articales)
for (const doctor of doctors){
await createDoctorElement(doctor.id,doctor.name,doctor.media,doctor.educational_certificate)
sr.reveal('.content');
sr.reveal('img',{origin:'top'});


}
function createDoctorElement(doctorId,doctorName,doctorImage,education){
  console.log(doctorImage)
  const newDoctorListItem=document.createElement('li');
  let id= newDoctorListItem.dataset.doctorId=doctorId;
const newDoctorImage=document.createElement('img');
newDoctorImage.src  =doctorImage[0].original_url;
const newDoctorContentDiv=document.createElement('div');
newDoctorContentDiv.classList.add('content');
const newDivP=document.createElement('div');
const newDoctorName=document.createElement('h2');
const newRequestBtn= document.createElement('a');
newRequestBtn.href=`/send_request.html?id=${id}`; 
newRequestBtn.textContent='Send Request';
newDoctorName.textContent=doctorName;
const newDoctorEducationP=document.createElement('p');
newDoctorEducationP.textContent=education;
newDoctorContentDiv.appendChild(newDoctorName);
newDivP.appendChild(newDoctorEducationP)
newDivP.appendChild(newRequestBtn);
newDoctorContentDiv.appendChild(newDivP);
newDoctorListItem.appendChild(newDoctorImage);
newDoctorListItem.appendChild(newDoctorContentDiv);
doctorOrderList.appendChild(newDoctorListItem);


}

}

doctorInformation();