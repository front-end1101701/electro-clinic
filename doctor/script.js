const urlParams = new URLSearchParams(window.location.search);

const user = urlParams.get('user');
const doctor=urlParams.get('doctor');
console.log(user,doctor);
let i=0;
let j=0;
let l=0
const chatMessages = document.getElementById('chat-messages');
const messageInput = document.getElementById('message-input');
const sendButton = document.getElementById('send-button');



// Add event listener to send button
sendButton.addEventListener('click', () => {
    sendMessage(),uploadFile();
});

// Add event listener to message input for "Enter" key press
messageInput.addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        sendMessage();
       
              
                  uploadFile();
                
    }
});

// Function to send a message
async function sendMessage() {
 
    const message = messageInput.value.trim();
   let massageData={
    user_id:user,
    doctor_id:doctor,
    massage:message,
   }


    try{
        response= await fetch('http://127.0.0.1:8000/api/doctor/send',{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(massageData)
        
        });
      }
      catch(error){
          alert('something go wrong!');
          return;
      }
     if(!response.ok){
      alert('something went wrong');
      return;
  }

        messageInput.value = '';
    // }
}

async function retrieveMassages(){
    let response;
    userData={
        user_id:user,
        doctor_id:doctor
    }
    try{
      response= await fetch(`http://127.0.0.1:8000/api/doctor/chat_index`,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(userData)
        
      });
    }
    catch(error){
        alert('something go wrong!');
        return;
    }
   if(!response.ok){
    alert('something went wrong');
    return;
}
const responseData= await response.json();

for (const massage of responseData){
  console.log(massage)
    if(massage.count>i){
    await createMassage(massage.massage,massage.who,massage.media, massage.created_at);
    i=massage.count;
    console.log(massage.media)
    // if(massage.media[0]!==undefined){
    // l=massage.media.length
  // }
    
  }else if (massage.media[0]!==undefined){
    console.log(j)
    for (let file of massage.media){
      if (file.id>j){
    const newMassageDiv=document.createElement('div');
    newMassageDiv.classList.add('message');
    const newImage=document.createElement('img')
    if(massage.who==1){
      newImage.classList.add('sent');
    }else{
      newImage.classList.add('recieve');
    }
    
    newImage.classList.add('file');
    newImage.src=file.original_url;
    newMassageDiv.appendChild(newImage);
    chatMessages.appendChild(newMassageDiv);
    j=file.id;
      }
    }
  }
}
async function createMassage(massage, who, media, time){
  const newMassageDiv=document.createElement('div');
  newMassageDiv.classList.add('message');
  const newMassageContentDiv=document.createElement('div');
  newMassageContentDiv.classList.add('message-content')
  if(who==1){
    newMassageContentDiv.classList.add('sent')
  }else{
    newMassageContentDiv.classList.add('received')
  }
  const newContentP=document.createElement('p');
  newContentP.textContent=massage;
  const newTimeSpan=document.createElement('span');
  newTimeSpan.classList.add('timestamp');
  newTimeSpan.textContent=time;


  newMassageContentDiv.appendChild(newContentP);
  newMassageContentDiv.appendChild(newTimeSpan);
  newMassageDiv.appendChild(newMassageContentDiv);
  if(media[0]!==undefined){
    for(let file of media){
      console.log(file)
      const newImage= document.createElement('img')
      if(who==1){
        newImage.classList.add('sent');
      }else{
        newImage.classList.add('recieve');
      }
      newImage.classList.add('file');
      newImage.src=file.original_url;
      j=file.id
      newMassageDiv.appendChild(newImage);

    }
  }
  chatMessages.appendChild(newMassageDiv);

  // chatMessages.scrollTop = chatMessages.scrollHeight;



}
}

async function uploadFile() {
  let response;
  const fileInput = document.getElementById("file-upload");
  const file = fileInput.files[0];

  if (file !== undefined) {
    console.log(file);

    const formData = new FormData();
    formData.append("file", file);
    console.log(formData);

    formData.append("user_id", user);
    formData.append("doctor_id", doctor);

    try {
      response = await fetch("http://127.0.0.1:8000/api/doctor/send_file", {
        method: 'POST',
        body: formData, // Pass formData directly as the body
      });
    } catch (error) {
      alert('Something went wrong!');
      return;
    }

    if (!response.ok) {
      alert('Something went wrong');
      return;
    }

   
  }
}






function fetchAndRefreshMessages() {
  retrieveMassages();
}


setInterval(fetchAndRefreshMessages, 5000);