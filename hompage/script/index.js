let articleListElement=document.querySelector('#article-list')
let doctorInformationListItem=document.querySelector('.swiper-wrapper')
let i=0;
let j=1;
const sr = ScrollReveal({
  origin: 'bottom',
  distance: '60px',
  duration: 2500,
  delay: 500,
  reset:true,
})



async function articleLoad()
{
    let response;
    try{
      response= await fetch('http://127.0.0.1:8000/api/chats/article_index',{
        method: 'GET',
        
      });
    }
    catch(error){
        alert('something go wrong!');
        return;
    }

const responseData = await response.json();
console.log(responseData)
const articales= responseData.articles
// console.log(articales)
for (const article of articales){
await createArticleElement(article.id,article.title,article.summary,article.media)
sr.reveal('.test');
sr.reveal('.test1',{origin:'right'});
sr.reveal('.test2',{origin:'left'});


}

function createArticleElement(articleId, articleTitle, articleSummary, articleMedia){
const newArticleElement= document.createElement('li');
  if(i<3){
    newArticleElement.classList.add('test');
  }else if(i<5){
    newArticleElement.classList.add('test1');
  }else if(i<7){
    newArticleElement.classList.add('test2');
  }else{
    newArticleElement.classList.add('test');
  }
  i++;

const newDivElement=document.createElement('div');
const newContentDivElement=document.createElement('div');
newDivElement.classList.add('content');
let id= newArticleElement.dataset.articleId=articleId;
const ArticleTitle= document.createElement('h2');
ArticleTitle.textContent=articleTitle;
const ArticleBody=document.createElement('p');
ArticleBody.textContent=articleSummary;

const ArticleImage= document.createElement('img')
ArticleImage.src = articleMedia[0].original_url;
// console.log(articleMedia.original_url);
const showArticle= document.createElement('a');
showArticle.href=`show.html?id=${articleId}`; 
// showArticle.href='http://127.0.0.1:8000/api/chats/'+articleId+'/show_article'
showArticle.textContent='READ MORE';
newArticleElement.appendChild(ArticleImage);
newContentDivElement.appendChild(ArticleTitle);
newContentDivElement.appendChild(ArticleBody);
newDivElement.appendChild( newContentDivElement)
newDivElement.appendChild(showArticle);
newArticleElement.appendChild(newDivElement);
articleListElement.appendChild(newArticleElement);


}
}

async function doctorInformation()
{
    let response;
    try{
      response= await fetch('http://127.0.0.1:8000/api/chats/doctor_information',{
        method: 'GET',
        
      });
    }
    catch(error){
        alert('something go wrong!');
        return;
    }
   if(!response.ok){
    alert('something went wrong');
    return;
}
const responseData = await response.json();
const doctors= responseData.doctor

// console.log(articales)
for (const doctor of doctors){
await createDoctorElement(doctor.id,doctor.name,doctor.media,doctor.educational_certificate)
}
function createDoctorElement(doctorId,doctorName,doctorImage,education){
  
  const newDoctorDiv=document.createElement('div');
  let id= newDoctorDiv.dataset.doctorId=doctorId;
newDoctorDiv.classList.add('swiper-slide');
newDoctorDiv.classList.add('swiper-slide--'+j);j++;
const newDoctorImage=document.createElement('img');
newDoctorImage.src  =doctorImage[0].original_url;
const newDoctorContentDiv=document.createElement('div');
const newDoctorName=document.createElement('h2');
newDoctorName.textContent=doctorName;
const newDoctorEducationP=document.createElement('p');
newDoctorEducationP.textContent=education;
newDoctorContentDiv.appendChild(newDoctorName);
newDoctorContentDiv.appendChild(newDoctorEducationP);
newDoctorDiv.appendChild(newDoctorImage);
newDoctorDiv.appendChild(newDoctorContentDiv);
doctorInformationListItem.appendChild(newDoctorDiv);


}

}

articleLoad();
doctorInformation();
// let token=localStorage.getItem('accessToken');
// console.log(token);
// let id=localStorage.getItem('id');
// console.log(id);
sr.reveal('.A_content',{origin:'bottom'});
sr.reveal('.A_img',{origin:'top'});


let clinicalAnchor=document.querySelector('#clinical').addEventListener('click',function(){
  if(localStorage.getItem('accessToken')==null||localStorage.getItem('id')==null){
    alert('you must register first!!!');
    return;
  }
  else{
    setTimeout(() => {
      window.location.href = '/request.html';
        }, 500); 
  }
})
