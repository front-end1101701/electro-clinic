const id=localStorage.getItem('id');
console.log(id)
let doctorDivItem=document.getElementById('container')
async function accptedRequest(){
    let response
    try {
        response = await fetch(`http://127.0.0.1:8000/api/chats/${id}/show_accepted_request`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });
      } catch (error) {
        alert('Something went wrong!');
        return;
      }
      
      if (!response.ok) {
        alert('Something went wrong!');
        return;
      }
      
      const responseData=await response.json();
      const accepted= responseData.accepted;
      if(accepted[0]==undefined){
        const newResponse=document.createElement('h1');
        newResponse.classList.add('response');
        newResponse.textContent='your request have not checked yet try to check later..';
        doctorDivItem.appendChild(newResponse);
        setTimeout(() => {
          window.location.href = '/hompage/index.html';
            }, 3500); 

      }
      for (const accept of accepted){
        const doctorId= accept.doctor_id
        getDoctor(doctorId)
      }
     async function getDoctor(doctorId){
        
            let response;
            try{
              response= await fetch(`http://127.0.0.1:8000/api/chats/${doctorId}/single_doctor`,{
                method: 'GET',
                
              });
            }
            catch(error){
                alert('something go wrong!');
                return;
            }
           if(!response.ok){
            alert('something went wrong');
            return;
        }
    
        const responseData = await response.json();
        const doctor= responseData.doctor;
        console.log(doctor)
         createDoctorCard(doctor[0].id,doctor[0].name,doctor[0].media,doctor[0].educational_certificate)
        
        function createDoctorCard(doctorId,doctorName,doctorImage,education){
            const newDivElement=document.createElement('div');
            newDivElement.classList.add('doctor')
            const newDivContent=document.createElement('div');
            newDivContent.classList.add('content');
            let id= doctorDivItem.dataset.doctorId=doctorId;
            const newDoctorImage=document.createElement('img');
            newDoctorImage.src  =doctorImage[0].original_url;
            const newDoctornameElement=document.createElement('h2')
            newDoctornameElement.textContent=doctorName;
            const newDoctorEducationElement=document.createElement('p');
            const newAnchor=document.createElement('a');
            newAnchor.textContent='start conversation';
            newAnchor.href=`conversation.html?id=${id}`;
            newDoctorEducationElement.textContent=education;
            newDivElement.appendChild(newDoctorImage);
            newDivContent.appendChild(newDoctornameElement);
            newDivContent.appendChild(newDoctorEducationElement);
            newDivContent.appendChild(newAnchor);
            newDivElement.appendChild(newDivContent);
            doctorDivItem.appendChild(newDivElement);

        }
        
        
     }
     

}

accptedRequest();