let token=localStorage.getItem('accessToken');



// Function to refresh the JWT access token
const refreshAccessToken = () => {
    // Make a request to the token refresh endpoint
    fetch('/api/auth/refresh', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {
        // Update the stored access token with the refreshed token
        localStorage.setItem('accessToken', data.token);
        console.log('Token refreshed successfully!');
      })
      .catch(error => {
        console.error('Token refresh failed:', error);
      });
  
    // Schedule the next token refresh in 60 minutes
    setTimeout(refreshAccessToken, 60 * 60 * 1000);
  };
  
  // Call the refreshAccessToken function to start the token refresh mechanism
  refreshAccessToken();